package tests

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/sizrr"
)

// TestSizrr_NormalizeAll tests all possible data found and transforms it
// into standardized sizes, so you can check how it behaves in the big picture.
// Those sizes that were not recognized are marked as TODO.
func TestSizrr_NormalizeAll(t *testing.T) {
	service := sizrr.New()
	fileBefore, err := os.ReadFile("testFiles/before.txt")
	if err != nil {
		t.Fatal(err)
	}
	beforeData := strings.Split(string(fileBefore), "\n")
	normalizedSlice := make([]string, 0, len(beforeData))
	for _, item := range beforeData {
		result := service.Normalize(item)
		resultStringSlice := make([]string, 0, len(result))
		if len(result) == 0 {
			normalizedSlice = append(normalizedSlice, "TODO")
			continue
		}

		for _, resultItem := range result {
			resultStringSlice = append(resultStringSlice, resultItem.Size)
		}

		normalizedSlice = append(normalizedSlice, strings.Join(resultStringSlice, " | "))
	}
	// uncomment to update global test if algorithm was updated
	//_ = os.WriteFile("testFiles/after.txt", []byte(strings.Join(normalizedSlice, "\n")), 0644)

	fileAfter, err := os.ReadFile("testFiles/after.txt")
	if err != nil {
		t.Fatal(err)
	}
	// compare each line instead of whole structs to be able say if normalization is correct or not
	for key, line := range strings.Split(string(fileAfter), "\n") {
		assert.Equal(t, line, normalizedSlice[key], beforeData[key]+" is `"+line+"`, should be `"+normalizedSlice[key]+"`")
	}
}

// BenchmarkSizrr_Normalize is just a benchmark to observe how fast or slow
// normalization is.
func BenchmarkSizrr_Normalize(b *testing.B) {
	service := sizrr.New()
	testValues := []string{
		" L  ",
		"xl / xxl",
		"112 - 116 cm",
		"LARGE",
		"EUR 46",
		"40/DD",
		"2YRS",
		"1-2 months",
	}
	for _, value := range testValues {
		b.Run(value, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				_ = service.Normalize(value)
			}
		})
	}
}
