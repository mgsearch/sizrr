package normalizer

import (
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/mgsearch/sizrr/standardSize"
)

const (
	small  = "s"
	medium = "m"
	large  = "l"
)

// isSML detects if size is SML (L, S, XXL and so on)
func (normalizer *Normalizer) isSML(text string) bool {
	// unfortunately there is no way to avoid regex here
	matched, err := regexp.MatchString(`(?i)^(\d?x*[sml])?(\s*[-/|]\s*)?(\d?x*[sml])?(\s*[-/|]\s*)?(\d?x*)?[sml]$`, text)
	if err != nil {
		return false
	}

	return matched
}

func (normalizer *Normalizer) processSML(text string, output *standardSize.SizeList) {
	tokens := normalizer.tokenizer.Tokenize(text)
	for _, word := range tokens {
		// basic S / M / L
		if word == small || word == medium || word == large {
			output.Add(normalizer.createSMLItem(word))
			continue
		}

		// detect X(XXX)S or 3XL
		wordRunes := []rune(word)
		runesLength := len(wordRunes)
		lastLetter := string(wordRunes[runesLength-1])
		if lastLetter == small || lastLetter == medium || lastLetter == large && string(wordRunes[runesLength-2]) == "x" {
			// XL or XXL -> keep
			if word == "x"+large || word == "xx"+large || word == "x"+small || word == "xx"+small {
				output.Add(normalizer.createSMLItem(word))
				continue
			}

			// 2XL or 2XS -> convert to xxl / xxs
			if word == "2x"+large {
				output.Add(normalizer.createSMLItem("XXL"))
				continue
			}
			if word == "2x"+small {
				output.Add(normalizer.createSMLItem("XXS"))
				continue
			}

			// 4XL format - keep if numeric
			// assuming that only first letter like 9XL is possible, 22XL is nonsense
			if runesLength >= 3 && unicode.IsDigit(wordRunes[0]) {
				// check that only Xs are inside 4XL so nonsense like 4XML will not be processed
				onlyXs := true
				for _, runeLetter := range wordRunes[1 : runesLength-1] {
					if runeLetter != []rune("x")[0] {
						onlyXs = false
						break
					}
				}
				if onlyXs {
					output.Add(normalizer.createSMLItem(word))
					continue
				}
			}

			// XXXXL (a lot of Xs) format -> convert to numeric
			allAreX := true
			xCounter := 0
			for key, runeLetter := range wordRunes {
				// last letter - dismiss
				if key >= runesLength-1 {
					break
				}

				// found another letter than "x" -> not standardized XXXXL
				if runeLetter != []rune("x")[0] {
					allAreX = false
					break
				}
				xCounter++
			}
			if allAreX {
				result := strconv.Itoa(xCounter) + "x" + lastLetter
				output.Add(normalizer.createSMLItem(result))
			}
		}
	}
}

func (normalizer *Normalizer) createSMLItem(word string) standardSize.Size {
	return standardSize.Size{Size: strings.ToUpper(word), Type: standardSize.SizeTypeSML}
}
