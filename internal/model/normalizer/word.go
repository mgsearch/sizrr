package normalizer

import (
	"strings"

	"gitlab.com/mgsearch/sizrr/standardSize"
)

// isSizeRepresentedByWord - there are a lot of sizes in source data written simply
// by word(s) like "LARGE". It solved the most primitive way possible and solves
// more languages (EN, CS, SK, SI, HR...) that were used as target of this library.
func (normalizer *Normalizer) isSizeRepresentedByWord(text string) (bool, standardSize.Size) {
	translations := map[string]standardSize.Size{
		"malý":    {Size: small, Type: standardSize.SizeTypeSML},
		"malá":    {Size: small, Type: standardSize.SizeTypeSML},
		"mala":    {Size: small, Type: standardSize.SizeTypeSML},
		"malé":    {Size: small, Type: standardSize.SizeTypeSML},
		"small":   {Size: small, Type: standardSize.SizeTypeSML},
		"majhna":  {Size: small, Type: standardSize.SizeTypeSML},
		"střední": {Size: medium, Type: standardSize.SizeTypeSML},
		"stredná": {Size: medium, Type: standardSize.SizeTypeSML},
		"stredné": {Size: medium, Type: standardSize.SizeTypeSML},
		"medium":  {Size: medium, Type: standardSize.SizeTypeSML},
		"velký":   {Size: large, Type: standardSize.SizeTypeSML},
		"velká":   {Size: large, Type: standardSize.SizeTypeSML},
		"veľká":   {Size: large, Type: standardSize.SizeTypeSML},
		"veľký":   {Size: large, Type: standardSize.SizeTypeSML},
		"large":   {Size: large, Type: standardSize.SizeTypeSML},
	}
	size, exists := translations[text]
	if exists {
		size.Size = strings.ToUpper(size.Size)
		return true, size
	}

	return false, standardSize.Size{}
}
