package normalizer

import (
	"regexp"
	"strconv"
	"strings"
)

// isNumber detects if input string could be basic number.
func (normalizer *Normalizer) isNumber(text string) (bool, float64) {
	floatNumber, err := strconv.ParseFloat(strings.Replace(text, ",", ".", 1), 64)
	if err != nil {
		return false, 0.0
	}

	return true, floatNumber
}

// isNumericRange detects if input could be numeric range. As there are other types of clothing sizes that can
// look like range expression (but is not - like trousers "34/32" or like kids sizes "112/116" (which per se
// is range but is commonly used as 1 relatively standard value))
func (normalizer *Normalizer) isNumericRange(text string) (isNumericRange bool, from int, to int) {
	regx := regexp.MustCompile(`^(\d+)\s*[-–]\s*(\d+)$`)
	matches := regx.FindStringSubmatch(text)

	// if it is trousers (34/32) or infant / child clothing (50/56, 112 - 116 etc.), do not take it as range
	isTrousersSize, _ := normalizer.isTrousers(text)
	isKidsSize, _, _ := normalizer.isBabiesAndKidsSize(text)

	// is range like 97 - 101 and is obviously not trousers
	if len(matches) == 3 && !isTrousersSize && !isKidsSize {
		from, err1 := strconv.Atoi(matches[1])
		to, err2 := strconv.Atoi(matches[2])
		if err1 == nil && err2 == nil {
			return true, from, to
		}
	}

	return false, 0, 0
}

// isFraction detects if there is an attempt of size expressed in fractions like "43 2/3"
func (normalizer *Normalizer) isFraction(text string) (isFraction bool, number float64) {
	regx := regexp.MustCompile(`^(\d+)\s*(\d)/(\d)$`) // only single digit in fraction, who will use 11/17 in clothes
	matches := regx.FindStringSubmatch(text)

	// is range like 96 - 100 and is obviously not trousers
	if len(matches) == 4 {
		base, err1 := strconv.ParseFloat(matches[1], 64)
		numerator, err2 := strconv.ParseFloat(matches[2], 64)
		denominator, err3 := strconv.ParseFloat(matches[3], 64)

		// all are numbers and first number is lower than second number (who will use fractions like 3/2 in clothing sizes?)
		if err1 == nil && err2 == nil && err3 == nil && numerator < denominator {
			return true, base + numerator/denominator
		}
	}

	return false, 0
}
