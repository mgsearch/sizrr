package normalizer

import (
	"regexp"
	"strconv"
)

// isBra if you take your slightly larger glasses (because the classic ones are dewy if someone says "bra"), this just
// detects "bra format" like 40/D. Inputs are as always various - 40D, 40 D, 40/D basically.
func (normalizer *Normalizer) isBra(text string) (isBra bool, braSize int, cupSize string) {
	// believe or not, regex is based on https://en.wikipedia.org/wiki/Bra_size :-) (I enjoyed this part of programming)
	regx := regexp.MustCompile(`^(\d+)(\s*|/?)(AA?|B|C|DD?|E|FF?|GG?|HH?|I|JJ?|KK?|LL?|MM?|N|O|P|Q|R)$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 4 {
		braSize, err := strconv.Atoi(matches[1])
		if err != nil {
			return false, 0, ""
		}

		return true, braSize, matches[3]
	}

	return false, 0, ""
}
