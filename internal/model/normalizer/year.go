package normalizer

import (
	"regexp"
	"strconv"
)

const (
	// words or abbrev. expression of years for multiple languages
	yearsWordsRegex = `(y|yrs|years?|rok(y|ů|ům|ov)?|let(a)?|godin(a|e)?)`
)

// isYear detects if input string is a year expression like "2YRS"
func (normalizer *Normalizer) isYear(text string) (bool, string) {
	regx := regexp.MustCompile(`(?i)^(\d+)\s*` + yearsWordsRegex + `$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 6 {
		return true, matches[1]
	}

	return false, ""
}

// isYearRange detects if input string is range of years like "0-1yrs"
func (normalizer *Normalizer) isYearRange(text string) (isYearRange bool, yearFrom int, yearTo int) {
	regx := regexp.MustCompile(`(?i)^(\d+)\s*[-–/|]\s*(\d+)\s*` + yearsWordsRegex + `$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 7 {
		yearFrom, err1 := strconv.Atoi(matches[1])
		yearTo, err2 := strconv.Atoi(matches[2])
		if err1 == nil && err2 == nil {
			return true, yearFrom, yearTo
		}
	}

	return false, 0, 0
}
