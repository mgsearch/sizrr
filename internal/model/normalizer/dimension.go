package normalizer

import (
	"regexp"
	"strings"
)

// isDimension detects if size was expressed as 2D or 3D dimension like "2.5 x 2.5 cm" etc.
func (normalizer *Normalizer) isDimension(text string) (bool, []string) {
	// unfortunately there is no way to avoid regex here
	regx := regexp.MustCompile(`(?i)^(\d+([.,]?\d+)?)(\s*x\s*)(\d+([.,]?\d+)?)((\s*x\s*)(\d+([.,]\d+)?))?$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 10 {
		result := []string{
			strings.Replace(matches[1], ".", ",", 1),
			strings.Replace(matches[4], ".", ",", 1),
		}
		if matches[8] != "" {
			result = append(result, strings.Replace(matches[8], ".", ",", 1))
		}

		return true, result
	}

	return false, nil
}
