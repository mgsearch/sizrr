package normalizer

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/mgsearch/sizrr/internal/model/tokenizer"
	"gitlab.com/mgsearch/sizrr/standardSize"
)

// NewNormalizer is just a constructor for Normalizer
func NewNormalizer() *Normalizer {
	return &Normalizer{
		tokenizer: tokenizer.NewTokenizer(),
	}
}

// Normalizer tries to normalize string input assuming that input contains clothes or shoes
// size expression and tries to standardize it to any of known size type.
type Normalizer struct {
	tokenizer *tokenizer.Tokenizer
}

func (normalizer *Normalizer) Normalize(text string) standardSize.SizeList {
	text = strings.TrimSpace(strings.ToLower(text))
	text = normalizer.cleanUp(text)
	output := standardSize.NewSizeList()

	// because the data are quite various (maybe not quite, but insanely various) we are very careful with tokenizing
	// at the first place because there are data like "34/32" (trousers), "85/DD" (bra size),
	// "112/116" or "size 112 - 116 cm" (kids size) which we do not want to split / tokenize at all so let's
	// process what we can at the first attempt
	normalizer.extractAndNormalizeMeasurement(text, &output)

	// OK, we are patient and still have nothing. We will search for commonly used pattern
	// "A (B)", e.g. "M (137-147 cm)" BEFORE we try to dismantle things by "/" or "-" (which will destroy valuable
	// information in patterns like "M/S (1 - 2 YRS)"). And we try to process each part separately.
	if output.Len() == 0 {
		regx := regexp.MustCompile(`(?i)^(.*)\s+\((.*)\)$`)
		matches := regx.FindStringSubmatch(text)
		if len(matches) == 3 {
			normalizer.extractAndNormalizeMeasurement(strings.TrimSpace(matches[1]), &output)
			normalizer.extractAndNormalizeMeasurement(strings.TrimSpace(matches[2]), &output)
		}
	}

	// same as above but parts divided by comma. It is not so easy to just split
	// the string by comma as it is often used as decimal separator... So we do it
	// at this part as we suppose all numbers and number ranges were already processed
	// and what's left is just compositions like "43, 44, 45, 46" or "M, size 112-118"
	if output.Len() == 0 {
		words := strings.Split(text, ", ") // notice the space, really important
		for _, word := range words {
			normalizer.extractAndNormalizeMeasurement(strings.TrimSpace(word), &output)
		}
	}

	// after all attempts above, there is still nothing there. Let's try to check if string is composed of multiple
	// types of measurements like "178 - 182 cm/M" and then try to run the job above one more time
	if output.Len() == 0 {
		words := strings.Split(text, "/") // just split by "/" as it is the most common way to merge multiple values
		for _, word := range words {
			normalizer.extractAndNormalizeMeasurement(strings.TrimSpace(word), &output)
		}
	}

	// after splitting by "/", still nothing... OK, the last attempt is "-". We can not split by dash before as it
	// is commonly used for ranges. At this moment, only long tail weirdos like "XS - 36" should remain so let's
	// process them all.
	if output.Len() == 0 {
		words := strings.Split(text, "-")
		for _, word := range words {
			normalizer.extractAndNormalizeMeasurement(strings.TrimSpace(word), &output)
		}
	}

	return output
}

// extractAndNormalizeMeasurement is some sort of "router" that tries to match several possible patterns on
// the input string and tries to decide what type of clothing size it should be.
func (normalizer *Normalizer) extractAndNormalizeMeasurement(text string, output *standardSize.SizeList) {
	// basic "pure" number like 68 or 42.5
	isPureNumber, floatNumber := normalizer.isNumber(text)
	if isPureNumber {
		output.Add(standardSize.Size{Size: fmt.Sprintf("%.0f", math.Round(floatNumber)), Type: standardSize.SizeTpeNumeric})
		return
	}

	// basic SML size
	if normalizer.isSML(text) {
		normalizer.processSML(text, output)
		return
	}

	// one size / universal etc.
	if normalizer.isOneSize(text) {
		output.Add(standardSize.Size{Size: "one size", Type: standardSize.SizeTypeOneSize})
		return
	}

	// single year
	isYear, year := normalizer.isYear(text)
	if isYear {
		if year == "1" {
			output.Add(standardSize.Size{Size: year + " year", Type: standardSize.SizeTypeYear})
			return
		} else {
			output.Add(standardSize.Size{Size: year + " years", Type: standardSize.SizeTypeYear})
			return
		}
	}

	// single month
	isMonth, month := normalizer.isMonth(text)
	if isMonth {
		if month == "1" {
			output.Add(standardSize.Size{Size: month + " month", Type: standardSize.SizeTypeMonth})
			return
		} else {
			output.Add(standardSize.Size{Size: month + " months", Type: standardSize.SizeTypeMonth})
			return
		}
	}

	// year range
	isYearRange, yearFrom, yearTo := normalizer.isYearRange(text)
	if isYearRange {
		for i := yearFrom; i <= yearTo; i++ {
			if i == 1 {
				output.Add(standardSize.Size{Size: strconv.Itoa(i) + " year", Type: standardSize.SizeTypeYearRange})
			} else {
				output.Add(standardSize.Size{Size: strconv.Itoa(i) + " years", Type: standardSize.SizeTypeYearRange})
			}
		}
		return
	}

	// month range
	isMonthRange, monthFrom, monthTo := normalizer.isMonthRange(text)
	if isMonthRange {
		for i := monthFrom; i <= monthTo; i++ {
			if i == 1 {
				output.Add(standardSize.Size{Size: strconv.Itoa(i) + " month", Type: standardSize.SizeTypeMonthRange})
			} else {
				output.Add(standardSize.Size{Size: strconv.Itoa(i) + " months", Type: standardSize.SizeTypeMonthRange})
			}
		}
		return
	}

	// bra size
	isBraSize, braSize, cupSize := normalizer.isBra(strings.ToUpper(text)) // back to upper is done because we have defined codes like 75DD
	if isBraSize {
		output.Add(standardSize.Size{Size: strconv.Itoa(braSize) + cupSize, Type: standardSize.SizeTypeBra})
		return
	}

	// numeric range
	isNumericRange, from, to := normalizer.isNumericRange(text)
	if isNumericRange {
		for i := from; i <= to; i++ {
			output.Add(standardSize.Size{Size: strconv.Itoa(i), Type: standardSize.SizeTypeNumericRange})
		}
		return
	}

	// translate "word" defined size like "LARGE"
	isSizeRepresentedByWord, size := normalizer.isSizeRepresentedByWord(text)
	if isSizeRepresentedByWord {
		output.Add(size)
		return
	}

	// dimensions like "9x13" or "4.5 x 4.5 x 7 cm"
	isDimension, dimensions := normalizer.isDimension(text)
	if isDimension {
		output.Add(standardSize.Size{Size: strings.Join(dimensions, " × "), Type: standardSize.SizeTypeDimension})
		return
	}

	// trouser sizes like W34/L32 based on official table size
	isTrousers, trouserSize := normalizer.isTrousers(text)
	if isTrousers {
		output.Add(standardSize.Size{Size: trouserSize, Type: standardSize.SizeTypeTrousers})
		return
	}

	// infant or child sizes like 50/56, 112/116, 112 - 116 cm etc.
	isBabyOrKidSize, from, to := normalizer.isBabiesAndKidsSize(text)
	if isBabyOrKidSize {
		output.Add(standardSize.Size{Size: strconv.Itoa(from) + "/" + strconv.Itoa(to), Type: standardSize.SizeTypeBabiesAndKids})
	}

	// numbers containing fractions like 43 1/2
	isFraction, floatNumber := normalizer.isFraction(text)
	if isFraction {
		output.Add(standardSize.Size{Size: fmt.Sprintf("%.0f", math.Round(floatNumber)), Type: standardSize.SizeTpeNumeric})
		return
	}
}

// cleanUp removes unnecessary stuff from input string like "EUR 46", "L regular"
func (normalizer *Normalizer) cleanUp(text string) string {
	text = strings.ReplaceAll(text, "\t", " ") // replace tab by space
	cleanWords := []string{
		"regular",
		"reg",
		"normal",
		"long",
		"short",
		"women",
		"men",
		"eur",
		"dívčí",
		"dětský",
		"detský",
		"dětská",
		"pánská",
		"pánský",
		"velikost",
		"cm",
		"mm",
	}

	for _, cleanWord := range cleanWords {
		text = strings.ReplaceAll(text, cleanWord, "")
	}

	return strings.TrimSpace(text)
}
