# Package normalizer

Package normalizer contains core normalization algorithm divided into "normalizing router" which detects the possible type of size and multiple particular normalizers responsible for correct type detection and processing of each type.
