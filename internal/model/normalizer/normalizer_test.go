package normalizer

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/sizrr/standardSize"
)

// TestNormalizer_Normalize test all possible inputs and its solutions if sent to normalizer.
func TestNormalizer_Normalize(t *testing.T) {
	cases := map[string][]standardSize.Size{
		// basic SML sizes
		"S":   {{Size: "S", Type: standardSize.SizeTypeSML}},
		"M":   {{Size: "M", Type: standardSize.SizeTypeSML}},
		"L":   {{Size: "L", Type: standardSize.SizeTypeSML}},
		" s ": {{Size: "S", Type: standardSize.SizeTypeSML}},
		"m ":  {{Size: "M", Type: standardSize.SizeTypeSML}},
		" l":  {{Size: "L", Type: standardSize.SizeTypeSML}},

		// extra SML sizes (both XXXX... or 4X...)
		"XL":          {{Size: "XL", Type: standardSize.SizeTypeSML}},
		"XXL":         {{Size: "XXL", Type: standardSize.SizeTypeSML}},
		"2XL":         {{Size: "XXL", Type: standardSize.SizeTypeSML}}, // converted to XXL (2 and less Xs are kept)
		"2xs":         {{Size: "XXS", Type: standardSize.SizeTypeSML}}, // converted to XXL (2 and less Xs are kept)
		"XXXL":        {{Size: "3XL", Type: standardSize.SizeTypeSML}}, // too many Xs, from XXXL (3x) and more normalized to numbered Xs
		"3XL":         {{Size: "3XL", Type: standardSize.SizeTypeSML}},
		"4XL":         {{Size: "4XL", Type: standardSize.SizeTypeSML}},
		"XXXXL":       {{Size: "4XL", Type: standardSize.SizeTypeSML}}, // converted
		"xs":          {{Size: "XS", Type: standardSize.SizeTypeSML}},
		" XXs":        {{Size: "XXS", Type: standardSize.SizeTypeSML}},
		"XXXS":        {{Size: "3XS", Type: standardSize.SizeTypeSML}},
		"XXXXS ":      {{Size: "4XS", Type: standardSize.SizeTypeSML}},
		"XXXXXXXXXXL": {{Size: "10XL", Type: standardSize.SizeTypeSML}}, // extreme but should work along the same pattern

		// nonsense values
		"XXXXWHATEVERS": {}, // invalid characters between XXX and size letters
		"4XWHATEVERS":   {}, // invalid characters between XXX and size letters

		// multiple values
		"L/XL":         {{Size: "L", Type: standardSize.SizeTypeSML}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		" L / XL":      {{Size: "L", Type: standardSize.SizeTypeSML}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		" l / xl":      {{Size: "L", Type: standardSize.SizeTypeSML}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		"XXXL/XXXXL":   {{Size: "3XL", Type: standardSize.SizeTypeSML}, {Size: "4XL", Type: standardSize.SizeTypeSML}},
		"L-XL":         {{Size: "L", Type: standardSize.SizeTypeSML}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		" L - XL":      {{Size: "L", Type: standardSize.SizeTypeSML}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		"xs|s|m":       {{Size: "XS", Type: standardSize.SizeTypeSML}, {Size: "S", Type: standardSize.SizeTypeSML}, {Size: "M", Type: standardSize.SizeTypeSML}},
		"XXXL / XXXXL": {{Size: "3XL", Type: standardSize.SizeTypeSML}, {Size: "4XL", Type: standardSize.SizeTypeSML}},

		// duplicity removal
		"M / M regular": {{Size: "M", Type: standardSize.SizeTypeSML}},
		"XXXL / XXXL":   {{Size: "3XL", Type: standardSize.SizeTypeSML}},
		"XXXL / 3xl":    {{Size: "3XL", Type: standardSize.SizeTypeSML}},
		"40 / 40DD":     {{Size: "40", Type: standardSize.SizeTpeNumeric}, {Size: "40DD", Type: standardSize.SizeTypeBra}}, // same number but one is different type - no duplicity removal

		// numeric type
		"68":         {{Size: "68", Type: standardSize.SizeTpeNumeric}},
		"41,5":       {{Size: "42", Type: standardSize.SizeTpeNumeric}},
		"41.5":       {{Size: "42", Type: standardSize.SizeTpeNumeric}},
		"40,5":       {{Size: "41", Type: standardSize.SizeTpeNumeric}}, // should be rounded
		"44,5":       {{Size: "45", Type: standardSize.SizeTpeNumeric}}, // should be rounded
		"46 EUR":     {{Size: "46", Type: standardSize.SizeTpeNumeric}}, // EUR is removed
		"46EUR":      {{Size: "46", Type: standardSize.SizeTpeNumeric}}, // EUR is removed
		"EUR 39 1/2": {{Size: "40", Type: standardSize.SizeTpeNumeric}}, // EUR is removed and fraction is rounded
		"EUR 39 1/3": {{Size: "39", Type: standardSize.SizeTpeNumeric}}, // EUR is removed and fraction is rounded
		"EUR 40 2/3": {{Size: "41", Type: standardSize.SizeTpeNumeric}}, // EUR is removed and fraction is rounded
		"7 7/8":      {{Size: "8", Type: standardSize.SizeTpeNumeric}},  // what the heck - some pipe or something but no space for you in clothes mate, sorry

		// universal
		"univerzální": {{Size: "one size", Type: standardSize.SizeTypeOneSize}},
		"UNI":         {{Size: "one size", Type: standardSize.SizeTypeOneSize}},
		"Univerzal":   {{Size: "one size", Type: standardSize.SizeTypeOneSize}},
		"ONE SIZE":    {{Size: "one size", Type: standardSize.SizeTypeOneSize}},

		// single year
		"1 YEAR":   {{Size: "1 year", Type: standardSize.SizeTypeYear}},
		"1Y":       {{Size: "1 year", Type: standardSize.SizeTypeYear}},
		"1 rok":    {{Size: "1 year", Type: standardSize.SizeTypeYear}},
		"3YRS":     {{Size: "3 years", Type: standardSize.SizeTypeYear}},
		"2Y":       {{Size: "2 years", Type: standardSize.SizeTypeYear}},
		"2 YRS":    {{Size: "2 years", Type: standardSize.SizeTypeYear}},
		"5 let":    {{Size: "5 years", Type: standardSize.SizeTypeYear}},
		"5 roků":   {{Size: "5 years", Type: standardSize.SizeTypeYear}},
		"8 YEARS":  {{Size: "8 years", Type: standardSize.SizeTypeYear}},
		"12 rokov": {{Size: "12 years", Type: standardSize.SizeTypeYear}}, // SK
		"6 godina": {{Size: "6 years", Type: standardSize.SizeTypeYear}},  // HR
		"4 godine": {{Size: "4 years", Type: standardSize.SizeTypeYear}},  // HR

		// single month
		"1 měsíc":     {{Size: "1 month", Type: standardSize.SizeTypeMonth}},
		"2 měsíce":    {{Size: "2 months", Type: standardSize.SizeTypeMonth}},
		"1MONTH":      {{Size: "1 month", Type: standardSize.SizeTypeMonth}},
		"2 months":    {{Size: "2 months", Type: standardSize.SizeTypeMonth}},
		"6 měsíců":    {{Size: "6 months", Type: standardSize.SizeTypeMonth}},
		"18M":         {{Size: "18 months", Type: standardSize.SizeTypeMonth}},
		"18 months":   {{Size: "18 months", Type: standardSize.SizeTypeMonth}},
		"4 mesiace":   {{Size: "4 months", Type: standardSize.SizeTypeMonth}},  // SK
		"12 mesiacov": {{Size: "12 months", Type: standardSize.SizeTypeMonth}}, // SK
		"12 mesecev":  {{Size: "12 months", Type: standardSize.SizeTypeMonth}}, // SI
		"1 mesec":     {{Size: "1 month", Type: standardSize.SizeTypeMonth}},   // SI
		"3 mesece":    {{Size: "3 months", Type: standardSize.SizeTypeMonth}},  // SI
		"1 mjeseci":   {{Size: "1 month", Type: standardSize.SizeTypeMonth}},   // HR

		// year ranges
		"0 - 3 YEAR":     {{Size: "0 years", Type: standardSize.SizeTypeYearRange}, {Size: "1 year", Type: standardSize.SizeTypeYearRange}, {Size: "2 years", Type: standardSize.SizeTypeYearRange}, {Size: "3 years", Type: standardSize.SizeTypeYearRange}},
		"0-1Y":           {{Size: "0 years", Type: standardSize.SizeTypeYearRange}, {Size: "1 year", Type: standardSize.SizeTypeYearRange}},
		"1 - 2 roků":     {{Size: "1 year", Type: standardSize.SizeTypeYearRange}, {Size: "2 years", Type: standardSize.SizeTypeYearRange}},
		"0/3YRS":         {{Size: "0 years", Type: standardSize.SizeTypeYearRange}, {Size: "1 year", Type: standardSize.SizeTypeYearRange}, {Size: "2 years", Type: standardSize.SizeTypeYearRange}, {Size: "3 years", Type: standardSize.SizeTypeYearRange}},
		"1-2Y":           {{Size: "1 year", Type: standardSize.SizeTypeYearRange}, {Size: "2 years", Type: standardSize.SizeTypeYearRange}},
		"2-4 YRS":        {{Size: "2 years", Type: standardSize.SizeTypeYearRange}, {Size: "3 years", Type: standardSize.SizeTypeYearRange}, {Size: "4 years", Type: standardSize.SizeTypeYearRange}},
		"12 - 18 let":    {{Size: "12 years", Type: standardSize.SizeTypeYearRange}, {Size: "13 years", Type: standardSize.SizeTypeYearRange}, {Size: "14 years", Type: standardSize.SizeTypeYearRange}, {Size: "15 years", Type: standardSize.SizeTypeYearRange}, {Size: "16 years", Type: standardSize.SizeTypeYearRange}, {Size: "17 years", Type: standardSize.SizeTypeYearRange}, {Size: "18 years", Type: standardSize.SizeTypeYearRange}},
		" 5 -  8  roků ": {{Size: "5 years", Type: standardSize.SizeTypeYearRange}, {Size: "6 years", Type: standardSize.SizeTypeYearRange}, {Size: "7 years", Type: standardSize.SizeTypeYearRange}, {Size: "8 years", Type: standardSize.SizeTypeYearRange}},
		"3-4 leta":       {{Size: "3 years", Type: standardSize.SizeTypeYearRange}, {Size: "4 years", Type: standardSize.SizeTypeYearRange}}, // SI

		// month ranges
		"0-1 měsíc":    {{Size: "0 months", Type: standardSize.SizeTypeMonthRange}, {Size: "1 month", Type: standardSize.SizeTypeMonthRange}},
		"0-2 měsíce":   {{Size: "0 months", Type: standardSize.SizeTypeMonthRange}, {Size: "1 month", Type: standardSize.SizeTypeMonthRange}, {Size: "2 months", Type: standardSize.SizeTypeMonthRange}},
		"0-1MONTH":     {{Size: "0 months", Type: standardSize.SizeTypeMonthRange}, {Size: "1 month", Type: standardSize.SizeTypeMonthRange}},
		"2 - 4 months": {{Size: "2 months", Type: standardSize.SizeTypeMonthRange}, {Size: "3 months", Type: standardSize.SizeTypeMonthRange}, {Size: "4 months", Type: standardSize.SizeTypeMonthRange}},
		"0-6 měsíců":   {{Size: "0 months", Type: standardSize.SizeTypeMonthRange}, {Size: "1 month", Type: standardSize.SizeTypeMonthRange}, {Size: "2 months", Type: standardSize.SizeTypeMonthRange}, {Size: "3 months", Type: standardSize.SizeTypeMonthRange}, {Size: "4 months", Type: standardSize.SizeTypeMonthRange}, {Size: "5 months", Type: standardSize.SizeTypeMonthRange}, {Size: "6 months", Type: standardSize.SizeTypeMonthRange}},
		"18-20M":       {{Size: "18 months", Type: standardSize.SizeTypeMonthRange}, {Size: "19 months", Type: standardSize.SizeTypeMonthRange}, {Size: "20 months", Type: standardSize.SizeTypeMonthRange}},
		"12/14 M":      {{Size: "12 months", Type: standardSize.SizeTypeMonthRange}, {Size: "13 months", Type: standardSize.SizeTypeMonthRange}, {Size: "14 months", Type: standardSize.SizeTypeMonthRange}},
		"18-20 months": {{Size: "18 months", Type: standardSize.SizeTypeMonthRange}, {Size: "19 months", Type: standardSize.SizeTypeMonthRange}, {Size: "20 months", Type: standardSize.SizeTypeMonthRange}},

		// bra sizes
		"95/B":  {{Size: "95B", Type: standardSize.SizeTypeBra}},
		"80 DD": {{Size: "80DD", Type: standardSize.SizeTypeBra}},
		"75 H":  {{Size: "75H", Type: standardSize.SizeTypeBra}},
		"85/GG": {{Size: "85GG", Type: standardSize.SizeTypeBra}},

		// clothing sizes with unnecessary details
		"M regular":   {{Size: "M", Type: standardSize.SizeTypeSML}},
		"42 Long":     {{Size: "42", Type: standardSize.SizeTpeNumeric}},
		"XXXL NORMAL": {{Size: "3XL", Type: standardSize.SizeTypeSML}},
		"120 cm":      {{Size: "120", Type: standardSize.SizeTpeNumeric}},
		"M Women":     {{Size: "M", Type: standardSize.SizeTypeSML}},
		"46 dívčí":    {{Size: "46", Type: standardSize.SizeTpeNumeric}},

		// numeric ranges
		"43 - 46": {{Size: "43", Type: standardSize.SizeTypeNumericRange}, {Size: "44", Type: standardSize.SizeTypeNumericRange}, {Size: "45", Type: standardSize.SizeTypeNumericRange}, {Size: "46", Type: standardSize.SizeTypeNumericRange}},
		"31-34":   {{Size: "31", Type: standardSize.SizeTypeNumericRange}, {Size: "32", Type: standardSize.SizeTypeNumericRange}, {Size: "33", Type: standardSize.SizeTypeNumericRange}, {Size: "34", Type: standardSize.SizeTypeNumericRange}},

		// sizes written by words
		"large":   {{Size: "L", Type: standardSize.SizeTypeSML}},
		" SMALL ": {{Size: "S", Type: standardSize.SizeTypeSML}},
		"Střední": {{Size: "M", Type: standardSize.SizeTypeSML}},
		"Veľká":   {{Size: "L", Type: standardSize.SizeTypeSML}}, // SK

		// combinations - multiple measurements in one string
		"190 - 192 cm/XL":         {{Size: "190", Type: standardSize.SizeTypeNumericRange}, {Size: "191", Type: standardSize.SizeTypeNumericRange}, {Size: "192", Type: standardSize.SizeTypeNumericRange}, {Size: "XL", Type: standardSize.SizeTypeSML}},
		"XS - 36":                 {{Size: "XS", Type: standardSize.SizeTypeSML}, {Size: "36", Type: standardSize.SizeTpeNumeric}},
		"M - 40":                  {{Size: "M", Type: standardSize.SizeTypeSML}, {Size: "40", Type: standardSize.SizeTpeNumeric}},
		"M (145-147 cm)":          {{Size: "M", Type: standardSize.SizeTypeSML}, {Size: "145", Type: standardSize.SizeTypeNumericRange}, {Size: "146", Type: standardSize.SizeTypeNumericRange}, {Size: "147", Type: standardSize.SizeTypeNumericRange}},
		"110–116 / 5–6 roků":      {{Size: "110/116", Type: standardSize.SizeTypeBabiesAndKids}, {Size: "5 years", Type: standardSize.SizeTypeYearRange}, {Size: "6 years", Type: standardSize.SizeTypeYearRange}}, // for those with slightly larger glasses look carefully at the "-" sign ;-)
		"28-29 cm, šířka:  12 mm": {{Size: "28", Type: standardSize.SizeTypeNumericRange}, {Size: "29", Type: standardSize.SizeTypeNumericRange}},
		"28-29 cm,	šířka:  12 mm": {{Size: "28", Type: standardSize.SizeTypeNumericRange}, {Size: "29", Type: standardSize.SizeTypeNumericRange}}, // notice hidden tab ;-)
		"43, 44, 45, 47": {{Size: "43", Type: standardSize.SizeTpeNumeric}, {Size: "44", Type: standardSize.SizeTpeNumeric}, {Size: "45", Type: standardSize.SizeTpeNumeric}, {Size: "47", Type: standardSize.SizeTpeNumeric}},

		// dimensions like 9x13 and 4.5 x 4.5 x 7 cm
		"9x13":                 {{Size: "9 × 13", Type: standardSize.SizeTypeDimension}},
		"100 x 65 cm ":         {{Size: "100 × 65", Type: standardSize.SizeTypeDimension}},
		"1.5 x 1.5 x 2 cm":     {{Size: "1,5 × 1,5 × 2", Type: standardSize.SizeTypeDimension}}, // dots are replaced with commas
		"1,5 X 1,5 X 2,5 cm":   {{Size: "1,5 × 1,5 × 2,5", Type: standardSize.SizeTypeDimension}},
		"1.5564 x 1.546x2.879": {{Size: "1,5564 × 1,546 × 2,879", Type: standardSize.SizeTypeDimension}}, // dots are replaced with commas

		// trousers - these values are obviously trouser sizes, not ranges
		"W33/L32":   {{Size: "W33/L32", Type: standardSize.SizeTypeTrousers}},
		"W33 / L32": {{Size: "W33/L32", Type: standardSize.SizeTypeTrousers}},
		"32/36":     {{Size: "W32/L36", Type: standardSize.SizeTypeTrousers}},
		"32/40":     {{Size: "W32/L40", Type: standardSize.SizeTypeTrousers}},
		"38/40":     {{Size: "W38/L40", Type: standardSize.SizeTypeTrousers}},

		// baby or kids size
		"122/126":               {{Size: "122/126", Type: standardSize.SizeTypeBabiesAndKids}},
		"122 / 126":             {{Size: "122/126", Type: standardSize.SizeTypeBabiesAndKids}},
		"122 - 126":             {{Size: "122/126", Type: standardSize.SizeTypeBabiesAndKids}},
		"122 - 126 cm":          {{Size: "122/126", Type: standardSize.SizeTypeBabiesAndKids}},
		"velikost 122 - 126 cm": {{Size: "122/126", Type: standardSize.SizeTypeBabiesAndKids}},
		"96-100 cm":             {{Size: "96/100", Type: standardSize.SizeTypeBabiesAndKids}},
		"98/104":                {{Size: "98/104", Type: standardSize.SizeTypeBabiesAndKids}},
		"50/54":                 {{Size: "50/54", Type: standardSize.SizeTypeBabiesAndKids}},
		"50/56":                 {{Size: "50/56", Type: standardSize.SizeTypeBabiesAndKids}},
		"170 / 176":             {{Size: "170/176", Type: standardSize.SizeTypeBabiesAndKids}},
	}

	normalizer := NewNormalizer()
	for input, shouldBe := range cases {
		result := normalizer.Normalize(input)
		assert.Equal(t, shouldBe, result.All(), fmt.Sprintf(`wrong result in test "%s"`, input))
	}
}

// TestNormalizer_isSimpleSML tests if detection of SML type works well.
func TestNormalizer_isSimpleSML(t *testing.T) {
	cases := map[string]bool{
		"S":             true,
		"M":             true,
		"L":             true,
		"s":             true,
		"m":             true,
		"l":             true,
		"XL":            true,
		"XXL":           true,
		"XXXL":          true,
		"3XL":           true,
		"4XL":           true,
		"XXXXL":         true,
		"xs":            true,
		"XXs":           true,
		"XXXS":          true,
		"XXXXS":         true,
		"XXXXXXXXXXL":   true,
		"XXXXWHATEVERS": false,
		"al":            false,
		"Universal":     false,
		"4XWHATEVERS":   false,
		"blbost":        false,
		"180 cm":        false,
		"L/XL":          true,
		"L / XL":        true,
		"l / xl":        true,
		"XXXL/XXXXL":    true,
		"L-XL":          true,
		"L - XL":        true,
		"xs|s|m":        true,
		"XXXL / XXXXL":  true,
	}

	normalizer := NewNormalizer()
	for input, shouldBe := range cases {
		result := normalizer.isSML(input)
		assert.Equal(t, shouldBe, result, fmt.Sprintf(`wrong result in test "%s"`, input))
	}
}
