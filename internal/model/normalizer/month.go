package normalizer

import (
	"regexp"
	"strconv"
)

const (
	// words or abbrev. expression of months for multiple languages
	monthsWordsRegex = `(m|month(s)?|měs|měsíc(e|ů|ům)?|mesiac(e|ov)?|mesec(e|ev)?|mjesec(i)?)`
)

// isMonth detects if string contains month expression like "1M"
func (normalizer *Normalizer) isMonth(text string) (bool, string) {
	regx := regexp.MustCompile(`(?i)^(\d+)\s*` + monthsWordsRegex + `$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 8 {
		return true, matches[1]
	}

	return false, ""
}

// isMonthRange detects if string is expression of month range like "0-2month"
func (normalizer *Normalizer) isMonthRange(text string) (isMonthRange bool, monthFrom int, monthTo int) {
	// unfortunately there is no way to avoid regex here
	regx := regexp.MustCompile(`(?i)^(\d+)(\s*[-–/|]\s*)(\d+)\s*` + monthsWordsRegex + `$`)
	matches := regx.FindStringSubmatch(text)
	if len(matches) == 10 {
		monthFrom, err1 := strconv.Atoi(matches[1])
		monthTo, err2 := strconv.Atoi(matches[3])
		if err1 == nil && err2 == nil {
			return true, monthFrom, monthTo
		}
	}

	return false, 0, 0
}
