package normalizer

import (
	"regexp"
	"strconv"
)

// isBabiesAndKidsSize detects infant measures like 50/56 and children measures like 112/116. There are various inputs
// '112 - 116', '112-116', '112 - 116 cm', 'size 112 - 116' etc., all should end as '112/116' (slash, no spaces).
func (normalizer *Normalizer) isBabiesAndKidsSize(text string) (isNumericRange bool, from int, to int) {
	regx := regexp.MustCompile(`^(\d+)\s*[-–/]\s*(\d+)$`)
	matches := regx.FindStringSubmatch(text)

	// is range like 96 - 100 and is obviously not trousers
	if len(matches) == 3 {
		from, err1 := strconv.Atoi(matches[1])
		to, err2 := strconv.Atoi(matches[2])

		// is number, first number is lower than second number exactly by 4 or 6 (this is most common range...
		// maybe there are more variant of ranges but these 2 are the most common and the rest is just a long tail)
		if err1 == nil && err2 == nil &&
			// first number is smaller than the second one
			from < to &&
			// the smallest baby size
			from >= 50 &&
			// the largest kid size
			to <= 180 &&
			// both are even numbers
			from%2 == 0 && to%2 == 0 &&
			// range is 4 or 6 in 112/116 or 110/116 as most common
			(to-from == 4 || to-from == 6) {
			return true, from, to
		}
	}

	return false, 0, 0
}
