package normalizer

// isOneSize detects if size is "one size" or "universal size" (which is common in data)
func (normalizer *Normalizer) isOneSize(text string) bool {
	// lowercase
	uniWords := map[string]bool{
		"univerzal":     true,
		"univerzální":   true,
		"univerzalni":   true,
		"nastavitelný":  true,
		"nastavitelná":  true,
		"one size":      true,
		"onesize":       true,
		"ONESIZE":       true,
		"uni":           true,
		"univerzalna":   true,
		"univerzalne":   true,
		"universal":     true,
		"prilagodljiva": true, // SI and HR
		"ena velikost":  true, // SI
	}
	_, isOneSize := uniWords[text]

	return isOneSize
}
