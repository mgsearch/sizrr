package tokenizer

import "strings"

// Tokenizer is simple tool that splits input string into tokens.
type Tokenizer struct {
}

// NewTokenizer is just a constructor for Tokenizer.
func NewTokenizer() *Tokenizer {
	return &Tokenizer{}
}

func (tokenizer *Tokenizer) Tokenize(text string) []string {
	words := strings.FieldsFunc(text, func(textRunes rune) bool {
		return textRunes == '/' ||
			textRunes == '|' ||
			textRunes == ',' ||
			textRunes == '-'
	})
	result := make([]string, 0, len(words))
	for _, word := range words {
		result = append(result, strings.TrimSpace(word))
	}

	return result
}
