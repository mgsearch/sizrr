# Sizrr - Clothes & Shoes Size Normalizer

##### by MALL Group Search Lab

Sizrr is a Golang tool / library that tries to understand **input clothes & shoes size(s)** (or basically input text) and **converts it into normalized sizes**, e.g. `"XXXL regular / EUR 47"` converts into
```json
[
  {
    "size": "3XL",
    "type": "SML"
  },
  {
    "size": "47",
    "type": "numeric"
  }
]
```

It is suitable for e.g. standardizing various clothes & shoes size inputs in ecommerce solutions (and if we say various, we mean [VARIOUS](#user-content-real-life-examples)). Resulting sizes more or less follow commonly used [cloth sizes](https://en.wikipedia.org/wiki/Clothing_sizes) (SML, numeric, babies & kids sizes, bras, trousers etc.). Complete list of recognized size types can be found [here](../standardSize/size.go#L6).

Some basic word inputs like *LARGE*, *uni* etc. are supported for English, Czech, Slovak, Slovenian and Croatian language.

If it fails to detect any type of size(s) in source string it just throws it away and returns an empty array.

## Installation

```
go get gitlab.com/mgsearch/sizrr
```

## Usage

```go
// init library
normalizer := sizrr.New()

// and get normalized colors
sizes := normalizer.Normalize(" XXXL  ") // 3XL
sizes = normalizer.Normalize("s/m") // S, M
sizes = normalizer.Normalize("M regular") // M
sizes = normalizer.Normalize("EUR 40 2/3") // 41
sizes = normalizer.Normalize("18M") // 18 months
sizes = normalizer.Normalize("0-1YRS") // 0 years, 1 year
sizes = normalizer.Normalize("40/DD") // 40DD (bra type)
sizes = normalizer.Normalize("112 - 116 cm") // 112/116 (babies and kids type)
sizes = normalizer.Normalize("SMALL / S") // S (deduplication)
sizes = normalizer.Normalize("32/36") // W32/L36 (trousers)
```

## Capabilities

- **case & spaces** insensitivenes: `"XXL"`, `"xxl"`, `"Xxl"`, `" XXL  "` converts into `["XXL"]`
- **SML** standardizing: `"XXXL"`, `"3XL"` converts into `["3XL"]` (from 3 "X" and above only number is preserved) 
- **multiple sizes** detection: `"M / L"`, `"M-L"`, `"M, L"` converts into `["M", "L"]`, even composition of different types like `"192 cm/XL"` converts into `["192", "XL"]`
- **variant simplification**: `"42 Long"` converts into `["42"]`, `"XL NORMAL"` converts into `["XL"]`, `"EUR 46"` converts into `["46"]`
- **months and years (and its ranges) recognition**: `"3YRS"` converts into `["3 years"]`, `"0-1Y"` converts into `["0 years", "1 year"]`, `"12/14 M"` converts into `["12 months", "13 months", "14 months"]`. Single values are preserved, months and years range is split into single values (as no standard exists in this case, sometimes there is 1 - 2YRS, sometimes there is 1 - 3YRS)
- **bra detection**: `"44/D"`, `"44 D"` and `"44D"` converts into `["44D"]`
- **trousers detection**: `"34/32"`, `"W34/L32"` converts into `["W34/L32"]`
- **babies & kids sizes detection**: `"112 - 116 cm"`, `"112 / 116"` converts into `["112/116"]`
- **rounding and fraction detection**: `"EUR 43.5"`, `"43 2/3"` converts into `["44"]`
- **basic word expressions**: `"SMALL"`, `" malý "`, `"Majhna"` converts into `["S"]`
- **universal size**: `"UNI"`, `"onesize"` and many others converts into `["one size"]`

<h4 id="various">Real Life Examples</h4>

- `["L"]` will be result of these (and many more) inputs:
> L, L Long, L normal, L REG, L Regular, L-blistr, L-L, L-WIDE, L/+, L/L, L/Long, L/Regular, L/T, L/XLong, large, velká, Velký
- `["XXL"]` will be result of these (and many more) inputs:
> 2XL, 2XL Long, 2XL Regular, XXL, XXL REG, XXL/Regular, XXL/t
- `["30"]` will be result of these (and many more) inputs:
> 29,5, 30, 29.5, 30 - 30, 30 - dívčí, 30 cm, 30 Regular, 30LONG, 30REG

## Benchmarks

<small>Linux, AMD64, Intel(R) Core(TM) i9-9900K CPU @ 3.60GHz</small>

| input            | ops | time |
|------------------| --- | --- |
| `" L  "`         | 49929 | 36784 ns/op |
| `"xl / xxl"`     | 47902 | 36588 ns/op |
| `"112 - 116 cm"` | 4753 | 322751 ns/op |
| `"LARGE"`        | 4908 | 276454 ns/op |
| `"EUR 46"`       | 1751180 | 645.5 ns/op |
| `"40/DD"`        | 6888 | 342987 ns/op |
| `"2YRS"`         | 18570 | 74107 ns/op |
| `"1-2 months"`   | 7515 | 234063 ns/op |

## Clothes & Shoes Size Normalization API

We provide **REST API** and **gRPC API**, see [gitlab.com/mgsearch/sizrr-server](https://gitlab.com/mgsearch/sizrr-server) for further details.

---
###### Sources:
<ul>
<li><small>https://en.wikipedia.org/wiki/Clothing_sizes</small></li>
<li><small>https://en.wikipedia.org/wiki/Bra_size</small></li>
</ul>

