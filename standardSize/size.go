package standardSize

const (
	// publicly available size types below are saved into Size.Type
	// and help to understand what type of size was recognized
	SizeTypeSML           = "SML"
	SizeTpeNumeric        = "numeric"
	SizeTypeNumericRange  = "numericRange"
	SizeTypeOneSize       = "oneSize"
	SizeTypeMonth         = "month"
	SizeTypeMonthRange    = "monthRange"
	SizeTypeYear          = "year"
	SizeTypeYearRange     = "yearRange"
	SizeTypeBra           = "bra"
	SizeTypeBabiesAndKids = "babiesAndKids"
	SizeTypeDimension     = "dimension"
	SizeTypeTrousers      = "trousers"
)

// Size is representative struct of normalized / standardized size.
type Size struct {
	// Size is name of normalized size like "XL" or "40DD"
	Size string `json:"size"`
	// Type is additional information and describes type the size is from like "SML" (XL), "numeric" (42), "bra" (40DD) etc.
	Type string `json:"type"`
}

// NewSizeList is just a constructor for SizeList
func NewSizeList() SizeList {
	return SizeList{
		sizes:  make([]Size, 0),
		exists: make(map[string]bool),
	}
}

// SizeList is primitive envelope above slice of Size which allows adding elements,
// checking if element exists etc.
type SizeList struct {
	sizes  []Size
	exists map[string]bool
}

// All gets all items.
func (list *SizeList) All() []Size {
	return list.sizes
}

// Add adds element (if not present). Operation is not thread-safe.
func (list *SizeList) Add(size Size) {
	key := size.Size + "_" + size.Type // deduplicate same values but only those of the same type
	_, exists := list.exists[key]
	if !exists {
		list.sizes = append(list.sizes, size)
		list.exists[key] = true
	}
}

// Len count of elements.
func (list *SizeList) Len() int {
	return len(list.sizes)
}
