package sizrr

import (
	"gitlab.com/mgsearch/sizrr/internal/model/normalizer"
	"gitlab.com/mgsearch/sizrr/standardSize"
)

// New is just a constructor for Sizrr
func New() *Sizrr {
	return &Sizrr{
		normalizer: normalizer.NewNormalizer(),
	}
}

// Sizrr is a tool that normalizes cloth sizes that come as input text. See README.md
// for full capabilities and usage.
type Sizrr struct {
	normalizer *normalizer.Normalizer
}

// Normalize takes input text e.g. "XXXXL / EUR 46 regular"
// and creates e.g. ["4XL", "46"]
func (service *Sizrr) Normalize(text string) []standardSize.Size {
	result := service.normalizer.Normalize(text)

	return result.All()
}
